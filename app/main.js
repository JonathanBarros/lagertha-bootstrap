import {bootstrap} from 'lagertha';
import 'routes';

function init() {
  document.body.insertAdjacentHTML('afterbegin', '<div id="client-body"></div>');
  bootstrap();
}

document.addEventListener('DOMContentLoaded', init);
