var url = require('url');
var path = require('path');
var fs = require('fs');
var defaultFile = 'index.html';
var folder = path.resolve(__dirname, '../build_dev');

module.exports = function (req, res, next) {
  var fileName = url.parse(req.url);
  fileName = fileName.href.split(fileName.search).join('');

  var fileExists = fs.existsSync(folder + fileName);

  if (!fileExists && fileName.indexOf('browser-sync-client') < 0 ){
    req.url = '/' + defaultFile;
  }
  return next();
};
