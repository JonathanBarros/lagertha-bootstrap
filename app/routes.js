import welcome from 'pages/welcome';
import {router} from 'lagertha';

const HOMEPAGE = /^\/$/;

router.on(HOMEPAGE, welcome.make);
