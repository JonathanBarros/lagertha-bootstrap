var IS_DEVELOPMENT = (process.argv.slice(2).indexOf('--p') > 0) ? false : true;

var path = require('path');
var webpack = require('webpack');
var BrowserSyncPlugin = require('browser-sync-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var pkg = require('./package.json');
var middleware = require('./scripts/middleware.js');

// config variables
var context = path.resolve(__dirname, "app");
var outputPath = IS_DEVELOPMENT ? 'build_dev' : 'build';
var sourceMaps = IS_DEVELOPMENT ? 'eval' : false;

module.exports = {
  watch: IS_DEVELOPMENT,
  context: context,
  devtool: sourceMaps,
  entry: {
    main: 'main.js'
  },
  output: {
    path: outputPath,
    publicPath: '/',
    filename: '[name].bundle.js'
  },
  resolve: {
    extensions: ['', '.js'],
    modulesDirectories: ['app', 'node_modules']
  },
  module: {
    preLoaders: [
      { test: /\.tag$/, exclude: /node_modules/, loader: 'riotjs-loader', query: { type: 'none' } },
    ],
    loaders: [
      {test: /\.css$/, loader: "style-loader!css-loader"},
      {test: /\.js|\.tag$/, exclude: /(node_modules)/, loader: "babel", query: { presets: ['es2015'] }},
      {test: /\.json$/, loader: 'json-loader'},
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: pkg.name,
      inject: 'body',
      version: pkg.version,
      buildDate: new Date().toUTCString()
    }),
    new BrowserSyncPlugin({
      open: false,
      ui: false,
      port: 8080,
      server: {
        baseDir: path.resolve(__dirname, outputPath),
        middleware: middleware
      }
    }),
    new webpack.DefinePlugin({
      DEVELOPMENT: IS_DEVELOPMENT,
      PRODUCTION: !IS_DEVELOPMENT,
      VERSION: JSON.stringify(pkg.version)
    }),
    new webpack.BannerPlugin(pkg.name + ' v' + pkg.version + ' | build: ' + new Date().toUTCString() + ' |  (c) 2015 Viking Makt')
  ]
};
