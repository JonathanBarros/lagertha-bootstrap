import './welcome.tag';
import {mount} from 'riot';

const version = 'v'+VERSION;

function make() {
  return mount('#client-body', 'lagertha-welcome', { version });
}

export default { make };
